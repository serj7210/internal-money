export interface Environment {
  production: boolean
  DbLoginUrl: string
  DbCreateUserUrl: string
  DbUserInfoUrl: string
  DbListTransUrl: string
  DbListUsers: string
}
